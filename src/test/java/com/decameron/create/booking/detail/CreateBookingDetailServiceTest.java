package com.decameron.create.booking.detail;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.camel.EndpointInject;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.spring.CamelSpringTestSupport;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.message.Message;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.decameron.xml.booking.Booking;
import com.decameron.xml.createbookingdetailrequest.CreateBookingDetailRequest;
import com.decameron.xml.createbookingdetailresponse.CreateBookingDetailResponse;

/**
 * 
 * @author Jerson Gomez
 *
 */
public class CreateBookingDetailServiceTest extends CamelSpringTestSupport {

    private static final Logger LOG = LoggerFactory
	    .getLogger(CreateBookingDetailServiceTest.class);

    private static final String PROPERTIES_FILE_DIR = "src/test/resources/";
    private static final String SERVER_URL = "http://localhost:8888";
    private static final String SERVICE = "/CreateBookingDetailService";

    @EndpointInject(uri = "mock:endRoute")
    protected MockEndpoint outputEndpoint;

    @Test
    public void test() throws Exception {
			LOG.info("******** Init Execution TEST Create Booking Detail *******");
			outputEndpoint.expectedMinimumMessageCount(1);

			SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd");			
			Date dateInitial = format.parse("2017-04-20");
			Date dateFinal = format.parse("2017-04-19");
			
			// create and set DTO
			CreateBookingDetailRequest request = new CreateBookingDetailRequest();
			request.setBookingCode(new BigInteger("29031327")); 
			request.setSystemCode("350");
			request.setEstablishmentCode(new BigInteger("8674"));
			request.setInitialDateDetail(dateInitial);
			request.setFinalDateDetail(dateFinal);
			request.setStatusCode(new BigInteger("311"));
			request.setExternalAgency(new BigInteger("6749"));
			request.setOriginCityCode(new BigInteger("3537"));
			request.setDestinationCityCode(new BigInteger("3552"));
			request.setPlanCode(new BigInteger("17486"));
			
			// create a CXF JaxWsProxyFactoryBean for creating JAX-WS proxies
			JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
			// set the HelloWorld portType class
			jaxWsProxyFactoryBean.setServiceClass(Booking.class);
			// set the address at which the HelloWorld endpoint will be called
			jaxWsProxyFactoryBean.setAddress(SERVER_URL + SERVICE);
		
			Map<String, Object> properties = new HashMap<String, Object>();
			properties.put("dataFormat", "PAYLOAD");
			jaxWsProxyFactoryBean.setProperties(properties);
			// create a JAX-WS proxy for the HelloWorld portType
			Booking service = (Booking) jaxWsProxyFactoryBean
				.create();
			
			ClientProxy.getClient(service).getRequestContext().put(Message.PROTOCOL_HEADERS, new HashMap<>());
			
			CreateBookingDetailResponse response = service.createBookingDetail(request);
	
    }

    @BeforeClass
    public static void setUpProperties() throws Exception {
    	LOG.info("******** setup *******");
		System.setProperty("karaf.home", PROPERTIES_FILE_DIR);
		System.setProperty("cxf.server", SERVER_URL);
    }

    @Override
    protected ClassPathXmlApplicationContext createApplicationContext() {
    	LOG.info("******** classpath *******");
    	return new ClassPathXmlApplicationContext("META-INF/spring/camel-context.xml");
    }

}
