
package com.decameron.xml.responsestructure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para responseStructure complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="responseStructure"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codeStatus"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="descriptionStatus"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "responseStructure", propOrder = {
    "codeStatus",
    "descriptionStatus"
})
public class ResponseStructure {

    @XmlElement(required = true)
    protected String codeStatus;
    @XmlElement(required = true)
    protected String descriptionStatus;

    /**
     * Obtiene el valor de la propiedad codeStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeStatus() {
        return codeStatus;
    }

    /**
     * Define el valor de la propiedad codeStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeStatus(String value) {
        this.codeStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad descriptionStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescriptionStatus() {
        return descriptionStatus;
    }

    /**
     * Define el valor de la propiedad descriptionStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescriptionStatus(String value) {
        this.descriptionStatus = value;
    }

}
