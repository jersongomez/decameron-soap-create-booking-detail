
package com.decameron.xml.createbookingdetailresponse;

import java.math.BigInteger;
import java.net.ConnectException;

import javax.validation.ConstraintViolation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.camel.ExchangeException;
import org.apache.camel.Handler;
import org.apache.camel.component.bean.validator.BeanValidationException;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.decameron.xml.responsestructure.ResponseStructure;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "responseStructure", "bookingDetailCode" })
@XmlRootElement(name = "createBookingDetailResponse")
public class CreateBookingDetailResponse {

	private static Log log = LogFactory.getLog(CreateBookingDetailResponse.class);

	@XmlElement(required = true)
	protected ResponseStructure responseStructure;
	protected BigInteger bookingDetailCode;

	/**
	 * Creacion de la estructura de respuesta a las exceptions
	 * 
	 */
	@Handler
	public CreateBookingDetailResponse factory(@ExchangeException Exception exception) {
		String descriptionStatus = "";

		switch (exception.getClass().getSimpleName()) {
		case "BeanValidationException":
			try {

				BeanValidationException beanException = (BeanValidationException) exception;

				for (ConstraintViolation<Object> constraintViolation : beanException.getConstraintViolations()) {
					descriptionStatus += constraintViolation.getPropertyPath() + " => "
							+ constraintViolation.getMessage() + ", ";
				}

			} catch (Exception e) {
				log.error(e);
				descriptionStatus = "No description error";
			}
			break;

		case "ConnectException":
			ConnectException excep2 = (ConnectException) exception;
			descriptionStatus = "Connection Error with the service";
			break;

		case "HttpOperationFailedException":
			HttpOperationFailedException excep3 = (HttpOperationFailedException) exception;
			descriptionStatus = "Response error in service, " + excep3.getStatusText() + " " + excep3.getStatusCode();
			break;

		default:
			descriptionStatus = "No description error";
			break;
		}

		ResponseStructure responseStructure = new ResponseStructure();
		responseStructure.setCodeStatus("NOK");
		responseStructure.setDescriptionStatus(descriptionStatus);
		this.setResponseStructure(responseStructure);
		return this;
	}

	/**
	 * Obtiene el valor de la propiedad responseStructure.
	 * 
	 * @return possible object is {@link ResponseStructure }
	 * 
	 */
	public ResponseStructure getResponseStructure() {
		return responseStructure;
	}

	/**
	 * Define el valor de la propiedad responseStructure.
	 * 
	 * @param value
	 *            allowed object is {@link ResponseStructure }
	 * 
	 */
	public void setResponseStructure(ResponseStructure value) {
		this.responseStructure = value;
	}

	/**
	 * Obtiene el valor de la propiedad bookingDetailCode.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getBookingDetailCode() {
		return bookingDetailCode;
	}

	/**
	 * Define el valor de la propiedad bookingDetailCode.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setBookingDetailCode(BigInteger value) {
		this.bookingDetailCode = value;
	}

}
