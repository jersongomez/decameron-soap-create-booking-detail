
package com.decameron.xml.createbookingdetailrequest;

import java.math.BigInteger;
import java.util.Date;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.validator.constraints.NotEmpty;

import com.decameron.annotated.validations.DateCompared;;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "systemCode",
    "bookingCode",
    "establishmentCode",
    "initialDateDetail",
    "finalDateDetail",
    "statusCode",
    "externalAgency",
    "originCityCode",
    "destinationCityCode",
    "planCode",
    "associatedDetail",
    "establishmentCentralCode",
    "isCTI",
    "isVilla",
    "amadeusDetailCode",
    "centralCode",
    "amadeusIdBlockPnr",
    "userHodeline",
    "servincluidosCode",
    "codeAirCentral",
    "centralPlanCode",
    "agencyBaseCode",
    "holdAgencyCode"
})

@XmlRootElement(name = "createBookingDetailRequest")
@DateCompared.List({
	@DateCompared(first = "initialDateDetail", second = "finalDateDetail", message = "finalDateDetail es inferior a initialDateDetail")
})
public class CreateBookingDetailRequest {

	@NotEmpty
    @XmlElement(required = true)
    protected String systemCode;
	
	@NotNull
    @XmlElement(required = true)
    protected BigInteger bookingCode;
    @XmlElement(required = true)
    protected BigInteger establishmentCode;
    
    @Future	
    @XmlElement(required = true)
    protected Date initialDateDetail;
    
    @Future
    @XmlElement(required = true)
    protected Date finalDateDetail;
    @XmlElement(required = true)
    protected BigInteger statusCode;
    protected BigInteger externalAgency;
    @XmlElement(required = true)
    protected BigInteger originCityCode;
    @XmlElement(required = true)
    protected BigInteger destinationCityCode;
    protected BigInteger planCode;
    protected BigInteger associatedDetail;
    protected BigInteger establishmentCentralCode;
    protected String isCTI;
    protected String isVilla;
    protected BigInteger amadeusDetailCode;
    protected String centralCode;
    protected String amadeusIdBlockPnr;
    protected BigInteger userHodeline;
    protected BigInteger servincluidosCode;
    protected String codeAirCentral;
    protected String centralPlanCode;
    protected BigInteger agencyBaseCode;
    protected BigInteger holdAgencyCode;

    /**
     * Obtiene el valor de la propiedad systemCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemCode() {
        return systemCode;
    }

    /**
     * Define el valor de la propiedad systemCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemCode(String value) {
        this.systemCode = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingCode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBookingCode() {
        return bookingCode;
    }

    /**
     * Define el valor de la propiedad bookingCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBookingCode(BigInteger value) {
        this.bookingCode = value;
    }

    /**
     * Obtiene el valor de la propiedad establishmentCode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getEstablishmentCode() {
        return establishmentCode;
    }

    /**
     * Define el valor de la propiedad establishmentCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setEstablishmentCode(BigInteger value) {
        this.establishmentCode = value;
    }

    /**
     * Obtiene el valor de la propiedad initialDateDetail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getInitialDateDetail() {
        return initialDateDetail;
    }

    /**
     * Define el valor de la propiedad initialDateDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInitialDateDetail(Date value) {
        this.initialDateDetail = value;
    }

    /**
     * Obtiene el valor de la propiedad finalDateDetail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getFinalDateDetail() {
        return finalDateDetail;
    }

    /**
     * Define el valor de la propiedad finalDateDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinalDateDetail(Date value) {
        this.finalDateDetail = value;
    }

    /**
     * Obtiene el valor de la propiedad statusCode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStatusCode() {
        return statusCode;
    }

    /**
     * Define el valor de la propiedad statusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStatusCode(BigInteger value) {
        this.statusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad externalAgency.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getExternalAgency() {
        return externalAgency;
    }

    /**
     * Define el valor de la propiedad externalAgency.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setExternalAgency(BigInteger value) {
        this.externalAgency = value;
    }

    /**
     * Obtiene el valor de la propiedad originCityCode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOriginCityCode() {
        return originCityCode;
    }

    /**
     * Define el valor de la propiedad originCityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOriginCityCode(BigInteger value) {
        this.originCityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad destinationCityCode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDestinationCityCode() {
        return destinationCityCode;
    }

    /**
     * Define el valor de la propiedad destinationCityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDestinationCityCode(BigInteger value) {
        this.destinationCityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad planCode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPlanCode() {
        return planCode;
    }

    /**
     * Define el valor de la propiedad planCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPlanCode(BigInteger value) {
        this.planCode = value;
    }

    /**
     * Obtiene el valor de la propiedad associatedDetail.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAssociatedDetail() {
        return associatedDetail;
    }

    /**
     * Define el valor de la propiedad associatedDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAssociatedDetail(BigInteger value) {
        this.associatedDetail = value;
    }

    /**
     * Obtiene el valor de la propiedad establishmentCentralCode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getEstablishmentCentralCode() {
        return establishmentCentralCode;
    }

    /**
     * Define el valor de la propiedad establishmentCentralCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setEstablishmentCentralCode(BigInteger value) {
        this.establishmentCentralCode = value;
    }

    /**
     * Obtiene el valor de la propiedad isCTI.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCTI() {
        return isCTI;
    }

    /**
     * Define el valor de la propiedad isCTI.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCTI(String value) {
        this.isCTI = value;
    }

    /**
     * Obtiene el valor de la propiedad isVilla.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsVilla() {
        return isVilla;
    }

    /**
     * Define el valor de la propiedad isVilla.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsVilla(String value) {
        this.isVilla = value;
    }

    /**
     * Obtiene el valor de la propiedad amadeusDetailCode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAmadeusDetailCode() {
        return amadeusDetailCode;
    }

    /**
     * Define el valor de la propiedad amadeusDetailCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAmadeusDetailCode(BigInteger value) {
        this.amadeusDetailCode = value;
    }

    /**
     * Obtiene el valor de la propiedad centralCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCentralCode() {
        return centralCode;
    }

    /**
     * Define el valor de la propiedad centralCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCentralCode(String value) {
        this.centralCode = value;
    }

    /**
     * Obtiene el valor de la propiedad amadeusIdBlockPnr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmadeusIdBlockPnr() {
        return amadeusIdBlockPnr;
    }

    /**
     * Define el valor de la propiedad amadeusIdBlockPnr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmadeusIdBlockPnr(String value) {
        this.amadeusIdBlockPnr = value;
    }

    /**
     * Obtiene el valor de la propiedad userHodeline.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getUserHodeline() {
        return userHodeline;
    }

    /**
     * Define el valor de la propiedad userHodeline.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setUserHodeline(BigInteger value) {
        this.userHodeline = value;
    }

    /**
     * Obtiene el valor de la propiedad servincluidosCode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getServincluidosCode() {
        return servincluidosCode;
    }

    /**
     * Define el valor de la propiedad servincluidosCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setServincluidosCode(BigInteger value) {
        this.servincluidosCode = value;
    }

    /**
     * Obtiene el valor de la propiedad codeAirCentral.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeAirCentral() {
        return codeAirCentral;
    }

    /**
     * Define el valor de la propiedad codeAirCentral.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeAirCentral(String value) {
        this.codeAirCentral = value;
    }

    /**
     * Obtiene el valor de la propiedad centralPlanCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCentralPlanCode() {
        return centralPlanCode;
    }

    /**
     * Define el valor de la propiedad centralPlanCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCentralPlanCode(String value) {
        this.centralPlanCode = value;
    }

    /**
     * Obtiene el valor de la propiedad agencyBaseCode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAgencyBaseCode() {
        return agencyBaseCode;
    }

    /**
     * Define el valor de la propiedad agencyBaseCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAgencyBaseCode(BigInteger value) {
        this.agencyBaseCode = value;
    }

    /**
     * Obtiene el valor de la propiedad holdAgencyCode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getHoldAgencyCode() {
        return holdAgencyCode;
    }

    /**
     * Define el valor de la propiedad holdAgencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setHoldAgencyCode(BigInteger value) {
        this.holdAgencyCode = value;
    }

}
