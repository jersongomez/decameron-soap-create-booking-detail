package com.decameron.annotated.validations;

import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateComparedValidator implements ConstraintValidator<DateCompared, Object> {

	private static final Logger LOG = LoggerFactory.getLogger(DateComparedValidator.class);

	private String initialDate;
	private String finalDate;
	private final String OLD_FORMAT = "E MMM dd HH:mm:ss Z yyyy";
	private final String NEW_FORMAT = "yyyy-MM-dd";

	@Override
	public void initialize(final DateCompared constraintAnnotation) {
		// Obtiene el name de los campos
		initialDate = constraintAnnotation.first();
		finalDate = constraintAnnotation.second();

	}

	@Override
	public boolean isValid(Object obj, ConstraintValidatorContext context) {

		try {
			initialDate = BeanUtils.getProperty(obj, initialDate).toString();
			finalDate = BeanUtils.getProperty(obj, finalDate).toString();

			DateFormat formatter = new SimpleDateFormat(OLD_FORMAT, Locale.US);						
			Date dateIn = formatter.parse(initialDate);
			Date dateFn = formatter.parse(finalDate);
			
			if(dateIn.compareTo(dateFn) > 0){
				return false;
			}
			
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}

}
