package com.decameron.processor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.decameron.xml.createbookingdetailrequest.CreateBookingDetailRequest;

public class ProcessorData implements Processor {

	private final String OLD_FORMAT = "E MMM dd HH:mm:ss Z yyyy";
	private final String NEW_FORMAT = "dd/MM/yyyy";
	private final Logger LOG = LoggerFactory.getLogger(ProcessorData.class);
	
	@Override
	public void process(Exchange exchange) throws Exception {
		
		CreateBookingDetailRequest myclass = exchange.getIn().getBody(CreateBookingDetailRequest.class);
		
		SimpleDateFormat format = new SimpleDateFormat(NEW_FORMAT);
		DateFormat formatter = new SimpleDateFormat(OLD_FORMAT, Locale.US);		
		String dateInitialf = format.format(formatter.parse(myclass.getInitialDateDetail().toString()));
		String dateFinalf = format.format(formatter.parse(myclass.getFinalDateDetail().toString()));
		
		exchange.getIn().setHeader("dateInitial", dateInitialf);
		exchange.getIn().setHeader("dateFinal", dateFinalf);					

	}

}
